package cn.codeforfun.http;

import okhttp3.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class GrimTest {
    @Test
    void name() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "token=40c09d2d9fb30fff3bbecf46397c8afc1a28184b59ed652beb00b612ede2686e");
        Request request = new Request.Builder()
                .url("https://www.grimtools.com/load_build.php?id=b28oK3yN")
                .method("POST", body)
                .addHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
                .addHeader("origin", "https://www.grimtools.com")
                .addHeader("x-requested-with", "XMLHttpRequest")
                .addHeader("Cookie", "__cfduid=d579cc13a9fe43a38029ee170282a927e1597888466; PHPSESSID=c406v8n29dvsajhdhol15nm7u2")
                .build();
        Response response = client.newCall(request).execute();

        System.out.println(response.body().string());
    }
}
