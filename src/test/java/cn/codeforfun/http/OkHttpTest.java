package cn.codeforfun.http;

import cn.codeforfun.utils.RegexUtils;
import cn.codeforfun.utils.StringUtils;
import okhttp3.*;
import org.junit.jupiter.api.Test;

import java.io.IOException;

public class OkHttpTest {
    @Test
    void test() throws IOException {
        String code = "b28oK3yN";
        String str1 = StringUtils.getRandomStringLower(43);
        String str2 = StringUtils.getRandomStringLower(26);

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();

        Request request = new Request.Builder()
                .url("https://www.grimtools.com/calc/" + code)
                .method("GET", null)
                .addHeader("Cookie", "__cfduid=" + str1 + "; PHPSESSID=" + str2)
                .build();
        Response response = client.newCall(request).execute();
        String token = RegexUtils.regex(response.body().string(), "^[\\s\\S]+token=\"(?<token>[\\s\\S]+)\";[\\s\\S]+$", "token");


        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "token=" + token);
        Request request1 = new Request.Builder()
                .url("https://www.grimtools.com/load_build.php?id=" + code)
                .method("POST", body)
                .addHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
                .addHeader("origin", "https://www.grimtools.com")
                .addHeader("x-requested-with", "XMLHttpRequest")
                .addHeader("referer", "https://www.grimtools.com/calc/" + code)
                .addHeader("accept-language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7")
                .addHeader("authority", "www.grimtools.com")
                .addHeader("dnt", "1")
                .addHeader("Cookie", "__cfduid=" + str1 + "; PHPSESSID=" + str2)
                .build();
        Response response1 = client.newCall(request1).execute();

        System.out.println(response1.body().string());

    }

    @Test
    void test1() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url("https://www.grimtools.com/calc/b28oK3yN")
                .method("GET", null)
                .addHeader("Cookie", "__cfduid=d1987869a2a5b49949a8d87bb3e7a4c5c1597803933; PHPSESSID=lo8rtk82dqrsna4t6vhla6sad1")
                .build();
        Response response = client.newCall(request).execute();
    }

    @Test
    void name2() throws IOException {
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=UTF-8");
        RequestBody body = RequestBody.create(mediaType, "token=7e5350f7adaa1f4f0c01c215e21d2b9b935499944053d7a4d6c69750f9e844df");
        Request request = new Request.Builder()
                .url("https://www.grimtools.com/load_build.php?id=b28oK3yN")
                .method("POST", body)
                .addHeader("content-type", "application/x-www-form-urlencoded; charset=UTF-8")
                .addHeader("origin", "https://www.grimtools.com")
                .addHeader("x-requested-with", "XMLHttpRequest")
                .addHeader("referer", "https://www.grimtools.com/calc/b28oK3yN")
                .addHeader("accept-language", "zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7")
                .addHeader("authority", "www.grimtools.com")
                .addHeader("dnt", "1")
                .addHeader("Cookie", "__cfduid=d1987869a2a5b49949a8d87bb3e7a4c5c1597803933; PHPSESSID=lo8rtk82dqrsna4t6vhla6sad1")
                .build();
        Response response = client.newCall(request).execute();

        System.out.println(response.body().string());
    }
}
