package cn.codeforfun.main;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.Resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(MainController.class)
class MainControllerTest {
    @Resource
    private MockMvc mockMvc;

    @Test
    void calc() throws Exception {
        String code = "b28oK3yN";
        mockMvc.perform(get("/calc/" + code))
                .andExpect(status().isOk())
                .andDo(print())
        ;
    }
}