package cn.codeforfun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGrimdawnTransferApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGrimdawnTransferApplication.class, args);
	}

}
