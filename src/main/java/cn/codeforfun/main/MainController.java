package cn.codeforfun.main;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;

/**
 * @author wangbin
 */
@RestController
public class MainController {

    @Resource
    private MainService mainService;

    @GetMapping("/calc/{code}")
    public ModelAndView calc(@PathVariable String code) throws IOException {
        return mainService.resolveSimple(code);
    }

    @GetMapping("/skill/{code}")
    public ModelAndView skill(@PathVariable String code) throws IOException {
        return mainService.resolveSkill(code);
    }

    /**
     * 通过code获取详情
     */
    @GetMapping("/all/{code}")
    public ModelAndView resolveAll(@PathVariable String code) throws IOException {
        return mainService.resolveAll(code);
    }


}
