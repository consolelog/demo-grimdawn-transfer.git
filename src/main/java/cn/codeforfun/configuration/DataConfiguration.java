package cn.codeforfun.configuration;

import cn.codeforfun.utils.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author wangbin
 */
@Configuration
public class DataConfiguration {
    @Bean("editorMap")
    public Map<String, String> editor() throws IOException {
        String editor = StringUtils.fromClasspath("static/editor.json");
        return new ObjectMapper().readValue(editor, HashMap.class);
    }

    @Bean("itemMap")
    public Map<String, String> item() throws IOException {
        String item = StringUtils.fromClasspath("static/zh_m.json");
        return new ObjectMapper().readValue(item, HashMap.class);
    }

    @Bean("calcJs")
    public String calcJs() throws IOException {
        return StringUtils.fromClasspath("static/calc.js");
    }

    @Bean("itemJs")
    public String itemJs() throws IOException {
        return StringUtils.fromClasspath("static/itemdb.js");
    }
}
